// import { Component } from '@angular/core';

// @Component({
//   selector: 'app-edit-profile',
//   templateUrl: './edit-profile.component.html',
//   styleUrls: ['./edit-profile.component.css']
// })
// export class EditProfileComponent {

//   urllink:string="insert/images/img3.jpg";

//   selectFiles(event:any){

//     if(event.target.files){

//       var reader=new FileReader()

//       reader.readAsDataURL(event.target.files[0])

//       reader.onload=(event:any)=>{

//         this.urllink=event.target.result

//       }

//     }

//   }
// }

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.css']
})
export class EditProfileComponent implements OnInit {
  user: any = {
    profilePic: '',
    followers: 100,
    following: 50,
    username: 'YourUsername',
    email: 'youremail@example.com',
    password: 'YourPassword',
    contactNumber: '123-456-7890'
  };

  editUsername = false;
  editEmail = false;
  editPassword = false;
  editContact = false;
  showMoreInfo = false;

  constructor() { }

  ngOnInit(): void {
  }

  editProfilePic() {
    // Add logic to edit profile picture here
  }

  viewFollowers() {
    // Add logic to view followers here
  }

  viewFollowing() {
    // Add logic to view following here
  }

  toggleMoreInfo() {
    this.showMoreInfo = !this.showMoreInfo;
  }

  cancel() {
    // Add logic for cancel button
  }

  saveChanges() {
    // Add logic to save changes to user profile here
  }
}

