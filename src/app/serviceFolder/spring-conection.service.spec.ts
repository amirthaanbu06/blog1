import { TestBed } from '@angular/core/testing';

import { SpringConectionService } from './spring-conection.service';

describe('SpringConectionService', () => {
  let service: SpringConectionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SpringConectionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
