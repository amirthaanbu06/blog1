import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Register } from '../model/register';
import { HttpClient } from '@angular/common/http';
import { Blog } from '../model/blog';
import { NumberFormatStyle } from '@angular/common';
@Injectable({
  providedIn: 'root'
})
export class SpringConectionService {
  getBlog: any;
  baseURL = 'http://localhost:8080'

  constructor(public httpClient:HttpClient) { }

  private URLRegistration="http://localhost:8080/restController1/register";
  createRegistration(blogRegister:any){
    return this.httpClient.post(this.URLRegistration,blogRegister);
  }

  private URLLogin="http://localhost:8080/restController1/login";
  loginuser(bloguser:any){
    return this.httpClient.post(this.URLLogin,bloguser);
  }

  private URLCreateBlog="http://localhost:8080/restController1/createblog";
  createBlog(createBlog:any){
    return this.httpClient.post(this.URLCreateBlog,createBlog);
  }

  // GetList(id:number){
  //   return this.httpClient.get(this.baseURL+'/restController1/list/'+id)
  // }


  private URLJavaCategory=this.baseURL+"/restController1/home/category/java";
  getAllJavaCat(){
    return this.httpClient.get(this.URLJavaCategory);
  }

  private URLPythonCategory=this.baseURL+"/restController1/home/category/python";
  getAllPythonCat(){
    return this.httpClient.get(this.URLPythonCategory);
  }

  private URLWebtechCategory=this.baseURL+"/restController1/home/category/webtech";
  getAllWebtechCat(){
    return this.httpClient.get(this.URLWebtechCategory);
  }

  private URLDbCategory=this.baseURL+"/restController1/home/category/db";
  getAllDbCat(){
    return this.httpClient.get(this.URLDbCategory);
  }

  private URLPopularArticles=this.baseURL+"/restController1/home/popular_articles";
  getPopulars(){
    return this.httpClient.get(this.URLPopularArticles);
  }

  private URLAllPopularArticles=this.baseURL+"/restController1/home/popular/see_all_articles";
  getAllPopulars(){
    return this.httpClient.get(this.URLAllPopularArticles);
  }

  private URLJavaArticles=this.baseURL+"/restController1/home/java";
  getJava(){
    return this.httpClient.get(this.URLJavaArticles);
  }

  private URLPythonArticles=this.baseURL+"/restController1/home/python";
  getPython(){
    return this.httpClient.get(this.URLPythonArticles);
  }

  private URLWebtechArticles=this.baseURL+"/restController1/home/webtech";
  getWebtech(){
    return this.httpClient.get(this.URLWebtechArticles);
  }

  private URLDbArticles=this.baseURL+"/restController1/home/database";
  getDb(){
    return this.httpClient.get(this.URLDbArticles);
  }


  getBlogDescription(blogTitle: any) {
    console.log(22222);

    const URLDescription = this.baseURL+"/restController1/home/blogs/"+blogTitle;
    console.log(67656);

    return this.httpClient.get(URLDescription);
  }

  getMyBlogs(userId: any) {
    console.log(22222);

    const URLDescription = this.baseURL+"/restController1/home/my_blogs/"+userId;
    console.log(67656);

    return this.httpClient.get(URLDescription);
  }


}
