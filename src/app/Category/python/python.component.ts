import { Component, OnInit } from '@angular/core';
import { Blog } from 'src/app/model/blog';
import { SpringConectionService } from 'src/app/serviceFolder/spring-conection.service';
import { Router ,ActivatedRoute} from '@angular/router';
import { BlogDTO } from 'src/app/model/dto';

@Component({
  selector: 'app-python',
  templateUrl: './python.component.html',
  styleUrls: ['./python.component.css']
})
export class PythonComponent implements OnInit{
  pythonCategory: BlogDTO[] = [];
  blogDesc: BlogDTO[] = [];
  SpringConectionService: any;
  item:any;
  constructor(private urlService:SpringConectionService,private router: Router){
}

ngOnInit() {
  this.getAllPythonCategory();

  }
  getBlogDescription(blogTitle: any) {
    this.router.navigate([`/blogs/${blogTitle}`])
  }
  
  getAllPythonCategory(){
    this.urlService.getAllPythonCat().subscribe((res:any)=>{
      this.pythonCategory.push(...res);
      console.log(this.pythonCategory)
    })
  }

}
