import { BlogDTO } from './../model/dto';
import { Component } from '@angular/core';
import { SpringConectionService } from '../serviceFolder/spring-conection.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-blog-description',
  templateUrl: './blog-description.component.html',
  styleUrls: ['./blog-description.component.css']
})
export class BlogDescriptionComponent {

  blogDesc: BlogDTO[] = [];
  blogTitle: any;

  constructor(private urlService:SpringConectionService,private router: ActivatedRoute){
    this.router.params.subscribe(params => {
      console.log(333333);

      this.blogTitle = params['blogTitle'];
    });
  }
  ngOnInit() {
    console.log(44444);

    this.getBlogDescription(this.blogTitle);
   }
   getBlogDescription(blogTitle: any) {
    console.log('entered blog desc');
    this.urlService.getBlogDescription(blogTitle).subscribe((res: any) => {
      this.blogDesc.push(...res);
      console.log(777777, res);
    });
  }
}
