import { Blog } from "./blog";

export class Register {
    id?:number;
    name?:string;
    email?:string;
    password?:string;
    contactNumber?:String;
    age?:String;
    dob?:String;
    address?:String;
    city?:String;
    state?:String;
    profile?:String;
    followers?:number;
    following?:number;

}
