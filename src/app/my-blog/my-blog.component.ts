import { CreateblogComponent } from '../createblog/createblog.component';
import { Component , OnInit} from '@angular/core';
import { Router ,ActivatedRoute} from '@angular/router';
import { Blog } from 'src/app/model/blog';
import { SpringConectionService } from 'src/app/serviceFolder/spring-conection.service';
import { BlogDTO } from '../model/dto';

@Component({
  selector: 'app-my-blog',
  templateUrl: './my-blog.component.html',
  styleUrls: ['./my-blog.component.css']
})
export class MyBlogComponent {
  myBlogs: BlogDTO[] = [];

  userId: any;

  constructor(private route:Router,private urlService:SpringConectionService,private router: ActivatedRoute){
    this.router.params.subscribe(params => {
      console.log(333333);

      this.userId = params['userId'];
    });
  }
  ngOnInit() {
    console.log(44444);

    this.getMyBlogs(this.userId);
   }
   getMyBlogs(userId: any) {
    console.log('entered blog desc');
    this.urlService.getMyBlogs(userId).subscribe((res: any) => {
      this.myBlogs.push(...res);
      console.log(777777, res);
    });
  }

  


 }
